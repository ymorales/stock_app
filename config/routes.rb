Rails.application.routes.draw do
  resources :articles
  resources :stores

  root 'stores#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :services do
    resources :stores, only: [:index] do

    end
    get 'articles' => '/services/articles#index'
    get 'articles/stores/:id' => '/services/articles#by_store'
  end

  get "/404" => "errors#not_found", :as => :not_found
  get "/500" => "errors#exception", :as => :error
  get "/400" => "errors#bad_request", :as => :bad_request
  get '*not_found', to: 'errors#not_found'

end
