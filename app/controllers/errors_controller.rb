class ErrorsController < ActionController::Base
  def not_found
    if env["REQUEST_PATH"] =~ /^\/services/
      respond_to do |format|
        format.json {
          render json: {
            error_msg:  'Route Not Found',
            error_code: 404,
            success:    false
          },
          status: 404
        }

        format.xml {
          render xml: {
            error_msg:  'Route Not Found',
            error_code: 404,
            success:    false
          }.to_xml(root: 'error'),
          status: 404
        }
      end
    else
      render text: "404 Not found", status: 404
    end
  end

  def exception
    if env["REQUEST_PATH"] =~ /^\/services/
      respond_to do |format|
        format.json {
          render json: {
            error_msg:  'Route Not Found',
            error_code: 500,
            success:    false
          },
          status: 500
        }

        format.xml {
          render xml: {
            error_msg:  'Route Not Found',
            error_code: 500,
            success:    false
          }.to_xml(root: 'error'),
          status: 500
        }
      end
    else
      render text: "500 Internal Server Error", status: 500
    end
  end

  def bad_request
      render text: "400 Bad request", status: 400
  end
end
