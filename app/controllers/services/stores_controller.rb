class Services::StoresController < Services::ApiController

  def index
    @stores = ::Store.all

    respond_to do |format|

      format.json {
        render json: {
          stores: ActiveModel::Serializer::CollectionSerializer.new(@stores, each_serializer: StoreSerializer),
          success: true,
          total_elements: @stores.count
        },
        status: 200
      }

      format.xml {
        render xml: {
          stores: @stores,
          success: true,
          total_elements: @stores.count
        }.to_xml(root: 'stores'),
        except: [:created_at, :updated_at],
        status: 200
      }
    end
  end

end
