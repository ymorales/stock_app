class Services::ArticlesController < Services::ApiController

  def index
    @articles = ::Article.all

    respond_to do |format|

      format.json {
        render json: {
          articles: ActiveModel::Serializer::CollectionSerializer.new(@articles, each_serializer: ArticleSerializer),
          success: true,
          total_elements: @articles.count
        },
        status: 200
      }

      format.xml {
        render xml: {
          articles: @articles,
          success: true,
          total_elements: @articles.count
        }.to_xml(root: 'articles'),
        except: [:created_at, :updated_at],
        status: 200
      }
    end
  end

  def by_store

    if /\d+/=~ params[:id]
      begin
        store = ::Store.find(params[:id])
        @articles = ::Article.where(store: store)

        respond_to do |format|

          format.json {
            render json: {
              articles: ActiveModel::Serializer::CollectionSerializer.new(@articles, each_serializer: ArticleSerializer),
              success: true,
              total_elements: @articles.count
            },
            status: 200
          }

          format.xml {
            render xml: {
              articles: @articles,
              success: true,
              total_elements: @articles.count
            }.to_xml(root: 'articles'),
            except: [:created_at, :updated_at],
            status: 200
          }
        end

      rescue ActiveRecord::RecordNotFound
        respond_to do |format|
          format.json {
            render json: {
              error_msg:  'Record Not Found',
              error_code: 404,
              success:    false
            },
            status: 404
          }

          format.xml {
            render xml: {
              error_msg:  'Record Not Found',
              error_code: 404,
              success:    false
            }.to_xml(root: 'error'),
            except: [:created_at, :updated_at],
            status: 404
          }
        end
      end
    else
      respond_to do |format|
        format.json {
          render json: {
            error_msg:  'Bad request',
            error_code: 400,
            success:    false
          },
          status: 400
        }

        format.xml {
          render xml: {
            error_msg:  'Bad request',
            error_code: 400,
            success:    false
          }.to_xml(root: 'error'),
          status: 400
        }
      end
    end

  end

end
