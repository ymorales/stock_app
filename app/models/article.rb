class Article < ApplicationRecord
  belongs_to :store
  validates :name, presence: true
  validates :price, presence: true, numericality: true
  validates :total_in_shelf, presence: true, numericality: true
  validates :total_in_vault, presence: true, numericality: true

end
