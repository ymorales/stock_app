class StoreSerializer < ActiveModel::Serializer
  attributes :id, :address, :name
end
