class ArticleSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :price, :total_in_shelf, :total_in_vault
  belongs_to :store
end
