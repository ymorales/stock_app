require 'capybara/rspec'

describe "stores interaction", :type => :feature do

  it "list stores" do
    visit '/stores'
    expect(page).to have_selector('table tr', :minimum => 0)
  end

  it "new store" do
    visit '/stores/new'
    fill_in 'Name', with: 'capybara store'
    fill_in 'Address', with: 'belgrano 2387'
    click_button 'Guardar tienda'
    expect(page).to have_content 'Store was successfully created.'
  end

  it "create store and visit it" do
    rspec_store = Store.create({name: 'Rspec store', address: 'Palo alto'})
    visit "/stores/#{rspec_store.id}"
    expect(page).to have_content 'Rspec store'
  end

  it "create store and editing it" do
    rails_store = Store.create({name: 'Rails store', address: 'silicon valley 235'})
    visit "/stores/#{rails_store.id}/edit"
    fill_in 'Name', with: 'Ruby on Rails store'
    fill_in 'Address', with: 'maure 2387'
    click_button 'Guardar tienda'
    expect(page).to have_content 'Store was successfully updated.'
  end

end
