def encoded_auth_credentials
  username = 'my_user'
  password = 'my_password'

  ActionController::HttpAuthentication::Basic
    .encode_credentials(username, password)
end

def http_login
  header 'Authorization', 'Basic bXlfdXNlcjpteV9wYXNzd29yZA=='
  header 'Accept', 'application/json'
  header 'Accept', 'application/xml'
end
