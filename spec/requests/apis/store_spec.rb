require "spec_helper"

describe "Stores API", type: :api do
  it 'retrieves a stores list' do
    http_login
    get "/services/stores"
    expect(last_response.status).to eql(200)
  end

  it 'un-authorized if headers are not seted' do
    get "/services/stores"
    expect(last_response.status).to eql(401)
  end

end
