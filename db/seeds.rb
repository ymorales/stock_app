# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
disney_store    = Store.create({name: 'Disney store', address: 'Magic kingdom'})
mac_store       = Store.create({name: 'Mac store', address: 'Av. corrientes 3230'})
starbucks_store = Store.create({name: 'Starbucks store', address: 'Gobernador ugarte 3455'})

mickey_mouse_mug = Article.create({
                    name: 'Mickey Mouse mug',
                    description: 'Beatiful mug',
                    price: 20,
                    total_in_shelf: 100,
                    total_in_vault: 2000,
                    store: disney_store
                  })

classic_ipod = Article.create({
                    name: 'iPod classic',
                    description: 'iPod first generation',
                    price: 200,
                    total_in_shelf: 500,
                    total_in_vault: 20000,
                    store: mac_store
                  })

starbucks_coffee = Article.create({
                    name: 'Colombian coffee cup',
                    description: 'Best america coffee cup',
                    price: 3,
                    total_in_shelf: 100,
                    total_in_vault: 2000,
                    store: starbucks_store
                  })
