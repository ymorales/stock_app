# README #

### Solution features ###
* Scaffolding for stores and articles
* RESTful API for stores and articles with JSON and XML support
* Rspec and capybara testing
* Haml templates with http://materializecss.com/
* I18n traslations
* Code coverage with simplecov

### Developed with ###
* ruby >= 2.0
* rails 5.0.0.1
* sqlite

### How to install ###
In your shell please execute

```
#!bash
$git clone https://ymorales@bitbucket.org/ymorales/stock_app.git stock
$cd stock
$bundle install
$rake db:migrate
== 20161021000517 CreateStores: migrating =====================================
-- create_table(:stores)
   -> 0.0020s
== 20161021000517 CreateStores: migrated (0.0021s) ============================

== 20161021015030 CreateArticles: migrating ===================================
-- create_table(:articles)
   -> 0.0226s
== 20161021015030 CreateArticles: migrated (0.0227s) ==========================
$rails db:seed
$rails s -p <port>
```

### Testing API ###
In your shell please execute
```
#!bash
$curl -X GET -H "Authorization: Basic bXlfdXNlcjpteV9wYXNzd29yZA=="  "http://localhost:<port>/services/stores"
{"stores":[{"id":1,"address":"Magic kingdom","name":"Disney store"},{"id":2,"address":"Av. corrientes 3230","name":"Mac store"},{"id":3,"address":"Gobernador ugarte 3455","name":"Starbucks store"}],"success":true,"total_elements":3}

$curl -X GET -H "Authorization: Basic bXlfdXNlcjpteV9wYXNzd29yZA=="  "http://localhost:3010/services/stores.xml"
<?xml version="1.0" encoding="UTF-8"?>
<stores>
  <stores type="array">
    <store>
      <id type="integer">1</id>
      <name>Disney store</name>
      <address>Magic kingdom</address>
      <created-at type="dateTime">2016-10-22T13:44:59Z</created-at>
      <updated-at type="dateTime">2016-10-22T13:44:59Z</updated-at>
    </store>
    <store>
      <id type="integer">2</id>
      <name>Mac store</name>
      <address>Av. corrientes 3230</address>
      <created-at type="dateTime">2016-10-22T13:44:59Z</created-at>
      <updated-at type="dateTime">2016-10-22T13:44:59Z</updated-at>
    </store>
    <store>
      <id type="integer">3</id>
      <name>Starbucks store</name>
      <address>Gobernador ugarte 3455</address>
      <created-at type="dateTime">2016-10-22T13:44:59Z</created-at>
      <updated-at type="dateTime">2016-10-22T13:44:59Z</updated-at>
    </store>
  </stores>
  <success type="boolean">true</success>
  <total-elements type="integer">3</total-elements>
</stores>

and so on....
```

### Testing Scaffolding ###
In your browser please visit http://localhost:port

![Captura de pantalla 2016-10-22 a las 10.50.53 a.m..png](https://bitbucket.org/repo/jpjn9R/images/3024227059-Captura%20de%20pantalla%202016-10-22%20a%20las%2010.50.53%20a.m..png)

![Captura de pantalla 2016-10-22 a las 10.52.30 a.m..png](https://bitbucket.org/repo/jpjn9R/images/19162604-Captura%20de%20pantalla%202016-10-22%20a%20las%2010.52.30%20a.m..png)

![Captura de pantalla 2016-10-22 a las 10.53.18 a.m..png](https://bitbucket.org/repo/jpjn9R/images/4270405298-Captura%20de%20pantalla%202016-10-22%20a%20las%2010.53.18%20a.m..png)

### Run tests ###
In your shell please execute

```
#!bash
$rspec
stores interaction
  list stores
  new store
  create store and visit it
  create store and editing it

Articles API
  retrieves an articles list
  un-authorized if headers are not seted
  retrieves an articles in a non-existant store
  retrieves an articles in a non-numerical store

Stores API
  retrieves a stores list
  un-authorized if headers are not seted

Finished in 1.89 seconds (files took 3.19 seconds to load)
10 examples, 0 failures
```

### Code coverage ###
![Captura de pantalla 2016-10-22 a las 10.59.56 a.m..png](https://bitbucket.org/repo/jpjn9R/images/4087042702-Captura%20de%20pantalla%202016-10-22%20a%20las%2010.59.56%20a.m..png)